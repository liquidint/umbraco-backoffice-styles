# Backoffice Styles - v8 #

Umbraco's Version 8 default Backoffice Theme features a salmon color that is very prominent (especially when using Nested Content). This package contains one simple CSS style sheet to override the default Umbraco Backoffice colors to be more in line with the Version 7 default colors. The style sheet can be easily modified to customize colors for each Umbraco instance.

Once this package is installed, the page will refresh, showing the new colors.

Files contained in the package will be installed to the App_Plugins/BackofficeStyles directory. Modify the backoffice.css file to customize your colors.